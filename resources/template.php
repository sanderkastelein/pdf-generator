<?php
function euro($s)
{
    if ((string)$s === "") {
        return "-"; // Als geen input, print "-" naar scherm.
    } else {
        $s = sprintf("€ %s", number_format($s, 2, ",", " "));
        return $s;
    }
}

?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>A simple, clean, and responsive HTML invoice template</title>
    <style>
        .invoice-box {
            max-width: 800px;
            margin: auto;
            padding: 30px;
            font-size: 16px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #000;
        }

        .invoice-box table {
            width: 100%;
            line-height: inherit;
            text-align: left;
        }

        .invoice-box table td {
            padding: 5px;
            vertical-align: top;
        }

        .invoice-box table tr td:nth-child(2) {
            text-align: right;
        }

        .invoice-box table tr.top table td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.top table td.title {
            font-size: 45px;
            line-height: 45px;
            color: #333;
        }

        .invoice-box table tr.information table td {
            padding-bottom: 40px;
        }

        .invoice-box table tr.heading td {
            background: #eee;
            border-bottom: 1px solid #ddd;
            font-weight: bold;
        }

        .invoice-box table tr.details td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.item td {
            border-bottom: 1px solid #eee;
        }

        .invoice-box table tr.item.last td {
            border-bottom: none;
        }

        .invoice-box table tr.total td:nth-child(2) {
            border-top: 2px solid #eee;
            font-weight: bold;
        }

        @media only screen and (max-width: 600px) {
            .invoice-box table tr.top table td {
                width: 100%;
                display: block;
                text-align: center;
            }

            .invoice-box table tr.information table td {
                width: 100%;
                display: block;
                text-align: center;
            }
        }

        /** RTL **/
        .rtl {
            direction: rtl;
            font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        }

        .rtl table {
            text-align: right;
        }

        .rtl table tr td:nth-child(2) {
            text-align: left;
        }

        .page-end {
            page-break-after: always;
        }

        footer {
            border-bottom: solid 10px #2556b8;
            border-top: solid 10px #f6c719;

            padding: 5px;
            position: absolute;
            bottom: 20px;
            left: 0;
            width: 100%;
            text-align: center;
            height: 22px;
        }



    </style>
</head>

<body>
<div class="invoice-box">
    <table cellpadding="0" cellspacing="0">
        <tr class="top">
            <td colspan="2">
                <table>
                    <tr>
                        <td class="title" style="width:283px;">
                            <img src="./logo.png" style="width:100%; max-width:300px;">
                        </td>

                        <td style="text-align: left;padding-left:50px;">
                            <strong>Offerte</strong><br>
                            Datum: <?php echo date("d-m-Y") ?><br>
                            Uw Naam: <?php echo $input['contact']['name'] . " " . $input['contact']['surname'] ?><br>
                            Uw Woonplaats: <?php echo $input['contact']['place']; ?> <br>
                            Uw E-mailadres: <?php echo $input['contact']['email'] ;?> <br>
                            Uw Telefoonnummer: <?php echo $input['contact']['phone'] ;?> <br>

                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="information">
            <td colspan="2">
                <table>
                    <tr>
                        <td>
                            Multi Keuken & Bad<br>
                            Elektraweg 7<br>
                            3144 CB Maassluis<br>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="heading">
            <td>
                Prijsopgave
            </td>

            <td>
                Prijs
            </td>
        </tr>

        <?php foreach ($input['price']['components'] as $component): ?>
            <tr class="details">
                <td>
                    <?php echo $component['label']; ?>
                </td>

                <td>
                    <?php echo euro($component['price']); ?>
                </td>
            </tr>
        <?php endforeach; ?>

        <tr class="total">
            <td></td>
            <td>
                Totaal: <?php echo euro($input['price']['total']); ?>
            </td>
        </tr>
    </table>
    <footer>
       Multi Keuken & Bad - Elektraweg 7 3144 CB Maassluis - T. 010 - 592 24 11 - F. 010 - 592 2602
    </footer>
</div>

<div class="page-end"></div>

<div class="invoice-box">
    <h2>Uw kast</h2>

    <table>
        <tbody>
        <tr>
            <td>Breedte</td>
            <td><?php echo $input['specs']['Breedte']; ?></td>
        </tr>
        <tr>
            <td>Hoogte</td>
            <td><?php echo $input['specs']['Hoogte']; ?></td>
        </tr>
        <tr>
            <td>Hoeveelheid deuren</td>
            <td><?php echo $input['specs']['Hoeveelheid deuren']; ?></td>
        </tr>
        <tr>
            <td>Positionering</td>
            <td><?php echo $input['specs']['Positionering']; ?></td>
        </tr>
        <tr>
            <td>Meetservice</td>
            <td><?php echo $input['specs']['Meetservice']; ?></td>
        </tr>
        <tr>
            <td>Montageservice</td>
            <td><?php echo $input['specs']['Montageservice']; ?></td>
        </tr>
        <tr>
            <td>Bezorgservice</td>
            <td><?php echo $input['specs']['Bezorgservice']; ?></td>
        </tr>
        </tbody>
    </table>

    <h2>Deuren &amp; Interieurs</h2>
    <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sapien justo, maximus sit amet enim a, congue dictum leo. Aenean eleifend porttitor elementum. Integer volutpat, purus ac malesuada sollicitudin, felis tellus lobortis libero, at ultrices mi felis vitae odio. Integer eget augue eu urna maximus commodo ac eget lectus. Proin bibendum erat sit amet diam fringilla, ut luctus ex semper. Pellentesque eros nibh, cursus vel lorem at, consectetur suscipit nibh. Phasellus fringilla, odio a elementum efficitur, dui sapien imperdiet urna, eu pharetra justo sem placerat lectus.


    </p>
    <?php foreach (["Deur 1", "Deur 2", "Deur 3", "Deur 4", "Deur 5"] as $deur): ?>
        <?php if ($input['specs'][$deur]): ?>
            <div style="page-break-inside: avoid; padding-top: 20px;">
                <div>
                    <h3><?php echo $deur; ?></h3>
                </div>
                <div>
                    <?php echo $input['specs'][$deur]['title'] ?>

                </div>
                <div>
                    <img height="300" src="<?php echo $input['specs'][$deur]['image'] ?>" alt="">

                    <?php if ($input['specs'][$deur]['indeling']): ?>
                        <img height="300" src="<?php echo $input['specs'][$deur]['indeling'] ?>" style="float:right;"
                             alt="">
                    <?php endif; ?>
                </div>
            </div>
        <?php endif; ?>
    <?php endforeach; ?>
</div>


</body>
</html>