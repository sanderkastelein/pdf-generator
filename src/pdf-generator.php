<?php

use Spatie\Browsershot\Browsershot;

require_once __DIR__."/../vendor/autoload.php";

$json = file_get_contents(__DIR__."/input.json");

function inputToPdf ($json){
	$input = json_decode($json, true);

	$input['specs']['Hoeveelheid deuren'] = 0;
	if(!empty($input['specs']['Deur 1'])) $input['specs']['Hoeveelheid deuren']++;
	if(!empty($input['specs']['Deur 2'])) $input['specs']['Hoeveelheid deuren']++;
	if(!empty($input['specs']['Deur 3'])) $input['specs']['Hoeveelheid deuren']++;
	if(!empty($input['specs']['Deur 4'])) $input['specs']['Hoeveelheid deuren']++;
	if(!empty($input['specs']['Deur 5'])) $input['specs']['Hoeveelheid deuren']++;

	$tmpFilename = __DIR__."/../resources/".uniqid().".html";
	touch($tmpFilename);
	$tmpFilename = realpath($tmpFilename);

	ob_start();
	include __DIR__."/../resources/template.php";
	$html = ob_get_clean();
	ob_clean();

	file_put_contents($tmpFilename, $html);

	$tmpFile = tempnam(sys_get_temp_dir(), 'docx2pdf');
	Browsershot::url('file://' . $tmpFilename)->setDelay(500)->savePdf($tmpFile);

	$returnValue = file_get_contents($tmpFile);
	unlink($tmpFile);
	unlink($tmpFilename);
	
	return $returnValue;
}